const uploaded = JSON.parse(localStorage.getItem('uploaded')) || [];

export const addUploaded = (giphyId) => {
  if (uploaded.find((id) => id === giphyId)) {
    // Giphy has already been added to uploaded
    return;
  }
  uploaded.push(giphyId);
  localStorage.setItem('uploaded', JSON.stringify(uploaded));
};

export const getUploaded = () => [...uploaded];
