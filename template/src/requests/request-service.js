import { API_KEY } from '../common/constants.js';
// import { formData } from '../events/helpers.js';

export const loadSingleGiphy = (id) => {
  const giphy = fetch(`http://api.giphy.com/v1/gifs/${id}?api_key=${API_KEY}`);
  return giphy;
};

export const loadGiphys = (searchTerm) => {
  const url = `http://api.giphy.com/v1/gifs/search?api_key=${API_KEY}&limit=40&q=${searchTerm}`;
  return fetch(url)
    .then((res) => res.json())
    .then((res) => {
      return res;
    });
};

export const loadTrendy = () => {
  const url = `http://api.giphy.com/v1/gifs/trending?api_key=${API_KEY}&limit=40`;
  return fetch(url)
    .then((res) => res.json())
    .then((res) => {
      return res;
    });
};

export const uploadGiphys = (formData) => {
  const URL = `https://upload.giphy.com/v1/gifs?api_key=${API_KEY}`;
  return fetch(URL, {
    method: 'POST',
    body: formData,
  }).then((r) => r.json());
};

