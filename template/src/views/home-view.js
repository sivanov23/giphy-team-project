export const toHomeView = () => `
<div id="home">
  <h1>Giphy Gif</h1>
  <div class="content">
    <p>Simple GIFS app. You can:</p>
    <ul>
      <li>Display Trending Gifs</li>
      <li>Search Gifs</li>
      <li>Display Gifs Details</li>
      <li>Upload Gifs</li>
      <li>Display Uploaded Gifs</li>
      <li>Favorite Gif</li>
    </ul>
  </div>
</div>
`;
