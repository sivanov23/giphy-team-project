import { toGiphySimple } from './giphy-views.js';

export const toFavoritesView = (giphys) => `
<div id="favorite-giphies">
  <h1 id="fav-giphy-title">Favorite giphies:</h1>
  <div class="favorites-view">
    ${giphys.map((giphy) => toGiphySimple(giphy)).join('\n') ||
     '<p>Add some giphies to favorites to see them here.</p>'}
  </div>
</div>
`;
