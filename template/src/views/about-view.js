export const toAboutView = () => `
<div id="about">
  <div class="content" id="about-content">
    <h1 class="about-title" id="about-text">About the app</h1>
    <h2 class="about-text" id="about-text">Authors:</h2>
    <p class="about-text id="about-text">Lyuben Shekerdjiev</p>
    <p class="about-text id="about-text">Simeon Ivanov</p>
    <p class="about-text id="about-text">Stoyan Stoyanov</p>
    <h2 class="about-text" id="about-text">Date:23.04.2021</h2>
    
  </div>
</div>
`;
