export const toUploadsView = () => `
<div class = "favorite-giphies">
  <p id="fav-giphy-title"> Choose gif to upload </label>
  <form id = "upload-form">
    <input type="file" name="file"accept=".gif" multiple>
    <input type="text" name="tags" placeholder="tags">
    <button type="submit" class="btn btn-primary" id="register-btn">
      Submit
    </button>
  </form>
</div>
`;
