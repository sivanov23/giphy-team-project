import { renderFavoriteStatus } from '../events/helpers.js';

export const toGiphysFromTrendyView = (giphy) => `
<div class="container" id="giphys">
  <h1 id="fav-giphy-title">${giphy} giphys:</h1>
  <div class="content" id="category-view">
    ${giphy.map(toGiphySimple).join('')}
  </div>
</div>
`;

export const toSingleGiphyView = (giphy) => `
<div>
${toGiphyDetailed(giphy)}
</div>
`;

export const toGiphySimple = (giphy) => {
  console.log(giphy);
  return `
 <div class="simple-giphy-container"> 
  <p id="single-giphy-title">${giphy.data.title}</p>
  <img src=${giphy.data.images.downsized.url}>
  <div id="view-details">
    <a href="#" class="giphy-simple" data-id="${giphy.data.id}">View details</a>
    <p id="heart">${renderFavoriteStatus(giphy.data.id)}</p>
  </div>
 </div>
`;
};

export const toGiphyDetailed = (giphy) =>{
  console.log(giphy);
  return `
    <div class="giphy-detailed">
      <div class="giphy-img">
        <img src=${giphy.data.images.downsized.url}">
      <div class="Giphy-info">
        <p>Uploader:${giphy.data.username || `Not Available`}</p>
        <p>ID:${giphy.data.id}</p>
        <p>Source:${giphy.data.source || `Not Available`}</p>
        <a href="#" class="back-to-trendy">Back</a>

    </div>
    `;
};

export const singleGiphy = (giphy) => `
<div class="favorites-view">
  <p id="single-giphy-title">${giphy.title}</p>
  <img src= "${giphy.images.downsized.url}">  
  <div id="view-details">
    <a href="#" class="giphy-simple" 
      data-id="${giphy.id}">View details</a>
    <p id="heart">${renderFavoriteStatus(giphy.id)}</p>
  </div>
</div>
`;
