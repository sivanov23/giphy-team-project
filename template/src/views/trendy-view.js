import { singleGiphy } from './giphy-views.js';

export const toTrendyView = (trendyGifs) => {
  return `<div class="gallery-view">
    ${trendyGifs.data.map((giphy) => singleGiphy(giphy)).join('')}
  </div>`;
};

