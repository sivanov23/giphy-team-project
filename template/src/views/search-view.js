import { singleGiphy } from './giphy-views.js';

export const toSearchView = (results) => {
  return `
    <div class="gallery-view">
    ${results.data.map((giphy) => singleGiphy(giphy)).join('')}
    </div>
      `;
};
