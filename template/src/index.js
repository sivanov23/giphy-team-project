import { HOME } from './common/constants.js';
import { toggleFavoriteStatus } from './events/favorites-events.js';
import { q } from './events/helpers.js';
import { loadPage, renderGiphyDetails }
  from './events/navigation-events.js';
import { renderSearchItems } from './events/search-events.js';

document.addEventListener('DOMContentLoaded', () => {
  // add global listener
  document.addEventListener('click', (e) => {
    // nav events
    if (e.target.classList.contains('nav-link')) {
      loadPage(e.target.getAttribute('data-page'));
    }

    // show giphy events
    if (e.target.classList.contains('giphy-simple')) {
      renderGiphyDetails(e.target.getAttribute('data-id'));
    }

    // toggle favorite event
    if (e.target.classList.contains('favorite')) {
      toggleFavoriteStatus(e.target.getAttribute('data-giphy-id'));
    }
  });

  // search events
  q('input#search').addEventListener('keypress', (e) => {
    if (e.key === 'Enter') {
      renderSearchItems(e.target.value);
      // document.addEventListener("scroll", e => {
      // checkScrollPosition()
      // })
    }
  });


  // const checkScrollPosition = () => {
  // const treshold = 400
  // if(document.body.clientHeight
  // s- window.scrollY - window.innerHeight < treshold){
  // }
  // }

  loadPage(HOME);
});


