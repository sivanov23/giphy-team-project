import {
  CONTAINER_SELECTOR, HOME, ABOUT, FAVORITES, UPLOAD, API_KEY, TRENDY, UPLOADED,
}
  from '../common/constants.js';
import { toHomeView } from '../views/home-view.js';
import { toSingleGiphyView }
  from '../views/giphy-views.js';
import { q, setActiveNav } from './helpers.js';
import { toAboutView } from '../views/about-view.js';
import { toTrendyView } from '../views/trendy-view.js';
import { toFavoritesView } from '../views/favorites-view.js';
import { getFavorites } from '../data/favorites.js';
import { loadTrendy } from '../requests/request-service.js';
import { loadSingleGiphy } from '../requests/request-service.js';
import { toUploadsView } from '../views/uploads-view.js';
import { uploadGiphys } from '../requests/request-service.js';
import { addUploaded, getUploaded } from '../data/uploaded.js';
import { toUploadedView } from '../views/uploaded-view.js';

// public API
export const loadPage = (page = '') => {
  const favorites = getFavorites();
  const trendy = loadTrendy();

  switch (page) {
  case HOME:
    setActiveNav(HOME);
    return renderHome();
  case ABOUT:
    setActiveNav(ABOUT);
    return renderAbout();
  case TRENDY:
    setActiveNav(TRENDY);
    return renderTrendy(trendy);
  case FAVORITES:
    setActiveNav(FAVORITES);
    return renderFavorites(favorites);
  case UPLOAD:
    setActiveNav(UPLOAD);
    return renderUpload();
  case UPLOADED:
    setActiveNav(UPLOADED);

    return renderUploaded();
  default: return null;
  }
};

export const renderGiphyDetails = (id = null) => {
  loadSingleGiphy(id)
    .then((res) => res.json())
    .then((giphy) => {
      q(CONTAINER_SELECTOR).innerHTML = toSingleGiphyView(giphy);
    });
};

const renderHome = () => {
  q(CONTAINER_SELECTOR).innerHTML = toHomeView();
};

const renderTrendy = (trendy) => {
  trendy.then((result) => {
    q(CONTAINER_SELECTOR).innerHTML = toTrendyView(result);
  });
};

const renderFavorites = () => {
  const favorites = getFavorites();
  const promises = favorites.map((id) => loadSingleGiphy(id).
    then((res) => res.json()));

  Promise
    .all(promises)
    .then((allGiphys) => {
      console.log(allGiphys);
      q(CONTAINER_SELECTOR).innerHTML = toFavoritesView(allGiphys);
    });
};

const renderUploaded = () => {
  const uploaded = getUploaded();
  const promises = uploaded.map((id) => loadSingleGiphy(id)
    .then((res) => res.json()));

  Promise
    .all(promises)
    .then((allGiphys) => {
      console.log(allGiphys);
      q(CONTAINER_SELECTOR).innerHTML = toUploadedView(allGiphys);
    });
};

const renderAbout = () => {
  q(CONTAINER_SELECTOR).innerHTML = toAboutView();
};

const renderUpload = () => {
  q(CONTAINER_SELECTOR).innerHTML = toUploadsView();
  document
    .querySelector('#register-btn')
    .addEventListener('click', (e) => {
      const form = q('#upload-form');
      const formData = new FormData(form);
      console.log(formData);
      formData.append('api_key', API_KEY);
      e.preventDefault();
      uploadGiphys(formData)
        .then((res) => {
          console.log(res);
          addUploaded(res.data.id);
        });
    });
};

