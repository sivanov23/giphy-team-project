import { CONTAINER_SELECTOR } from '../common/constants.js';
import { loadGiphys } from '../requests/request-service.js';
// import { loadGiphysByScroll } from '../requests/request-service.js';
import { toSearchView } from '../views/search-view.js';
import { q } from './helpers.js';
export const renderSearchItems = (searchTerm) => {
  loadGiphys(searchTerm)
    .then((res) => {
      q(CONTAINER_SELECTOR).innerHTML = toSearchView(res, searchTerm);
    });
};

